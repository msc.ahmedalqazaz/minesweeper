package be.kdg.minesweeper.view.main;

import be.kdg.minesweeper.model.Cell;
import be.kdg.minesweeper.model.Difficulty;
import be.kdg.minesweeper.model.Game;
import be.kdg.minesweeper.view.Tile;
import be.kdg.minesweeper.view.gameover.GameOverPresenter;
import be.kdg.minesweeper.view.gameover.GameOverView;
import be.kdg.minesweeper.view.sound.SoundPlayer;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Duration;

import java.util.List;

public class MainPresenter {
    private Game model;
    private final MainView view;
    private Timeline timer;

    public MainPresenter(Game model, MainView view) {
        this.model = model;
        this.view = view;

        this.addEventHandlers();
    }

    private void addEventHandlers() {
        this.view.getMenuItemRestart().setOnAction(event ->
                newGame(this.model.getDifficulty())
        );

        this.view.getMenuItemQuit().setOnAction(event ->
                Platform.exit()
        );

        this.view.getMenuItemEasy().setOnAction(event -> {
            if (this.model.getDifficulty() != Difficulty.EASY) {
                newGame(Difficulty.EASY);
            }
        });

        this.view.getMenuItemMedium().setOnAction(event -> {
            if (this.model.getDifficulty() != Difficulty.MEDIUM) {
                newGame(Difficulty.MEDIUM);
            }
        });

        this.view.getMenuItemHard().setOnAction(event -> {
            if (this.model.getDifficulty() != Difficulty.HARD) {
                newGame(Difficulty.HARD);
            }
        });

        this.view.getMenuItemAbout().setOnAction(event -> showAboutWindow());

        addCellEventHandlers();
    }

    private static void showAboutWindow() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Minesweeper JavaFX");
        alert.setContentText("""
          Instructions:
            Left click to reveal a cell
            Right click will change from empty (unrevealed)  => flag => ? => empty
            
            Made by Lars Willemsens.
            ©KdG University of Applied Sciences and Arts""");
        alert.setResizable(true); // This is needed because non-resizable windows are bugged under Linux...
        alert.showAndWait();
    }

    private void addCellEventHandlers() {
        for (int i = 0; i < view.getCells().length; i++) {
            for (int j = 0; j < view.getCells()[i].length; j++) {
                view.getCells()[i][j].setOnMousePressed(new MoveEventHandler(i, j));
            }
        }
    }

    private void updateView(int row, int column) {
        final Cell cell = this.model.getCell(row, column);
        final ImageView imageView = this.view.getCells()[row][column];
        imageView.setImage(Tile.get(cell, this.model.getNeighbouringBombs(cell.getRow(), cell.getColumn())).getImage());
    }

    private void gameOver() {
        if (this.timer != null) {
            this.timer.stop();
        }

        GameOverView gameOverView = new GameOverView();
        GameOverPresenter gameOverPresenter = new GameOverPresenter(this.model, gameOverView);
        Stage gameOverStage = new Stage();
        gameOverStage.setTitle("Game Over");
        gameOverStage.initModality(Modality.APPLICATION_MODAL);
        gameOverStage.setScene(new Scene(gameOverView));
        // gameOverStage.setResizable(false);  --- bugged under Linux...
        gameOverStage.sizeToScene();
        gameOverStage.setX( this.view.getScene().getWindow().getX() + this.view.getScene().getWindow().getWidth());
        gameOverStage.setY(this.view.getScene().getWindow().getY());
        gameOverStage.showAndWait();



        if (gameOverPresenter.shouldRestartGame()) {
            newGame(this.model.getDifficulty());
        } else {
            Platform.exit();
        }
    }

    private void newGame(Difficulty difficulty) {
        if (this.timer != null) {
            this.timer.stop();
        }

        this.model = new Game(difficulty);
        this.view.setUpGame(difficulty);
        this.addCellEventHandlers();

        Window window = this.view.getScene().getWindow();
        double originalWidth = window.getWidth();
        double originalHeight = window.getHeight();

        window.sizeToScene();

        double updatedX = window.getX() + originalWidth / 2 - window.getWidth() / 2;
        if (updatedX < 0.0) {
            updatedX = 0.0;
        }
        double updatedY = window.getY() + originalHeight / 2 - window.getHeight() / 2;
        if (updatedY < 0.0) {
            updatedY = 0.0;
        }

        window.setX(updatedX);
        window.setY(updatedY);
    }

    private class MoveEventHandler implements EventHandler<MouseEvent> {
        private final int row;
        private final int column;

        public MoveEventHandler(int row, int column) {
            this.row = row;
            this.column = column;
        }

        @Override
        public void handle(MouseEvent event) {
            if (!model.isFinished() &&
                !model.isRevealed(row, column)) {
                if (event.getButton() == MouseButton.PRIMARY) {
                    boolean wasStarted = model.isStarted();

                    revealCells(model.play(row, column));

                    if (model.isFinished()) {
                        gameOver();
                    }
                    if (!wasStarted && model.isStarted()) {
                        timer = new Timeline(new KeyFrame(
                            Duration.millis(1000),
                            actionEvent -> {
                                model.increaseTimer();
                                view.setTimer(model.getSeconds());
                            }));
                        timer.setCycleCount(Animation.INDEFINITE);
                        timer.play();
                    }
                } else if (event.getButton() == MouseButton.SECONDARY) {
                    model.changePrediction(row, column);
                    SoundPlayer.playBlipSound();
                    updateView(row, column);
                }
            }
        }

        private void revealCells(List<Cell> cellsToReveal) {
            if (cellsToReveal.size() > 1) {
                if (cellsToReveal.get(0).hasBomb()) {
                    SoundPlayer.playExplosionSound();
                } else {
                    SoundPlayer.playRevealSound();
                }
            }
            for (Cell cell : cellsToReveal) {
                updateView(cell.getRow(), cell.getColumn());
            }
        }
    }
}
