package be.kdg.minesweeper;

import be.kdg.minesweeper.model.Difficulty;
import be.kdg.minesweeper.model.Game;
import be.kdg.minesweeper.view.main.MainView;
import be.kdg.minesweeper.view.main.MainPresenter;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Minesweeper extends Application {
    @Override
    public void start(Stage primaryStage) {
        final Game model = new Game(Difficulty.EASY);
        final MainView view =  new MainView(model.getDifficulty());
        new MainPresenter(model, view);
        final Scene scene = new Scene(view);
        primaryStage.setTitle("Minesweeper");
        primaryStage.setScene(scene);
        primaryStage.sizeToScene();
        // primaryStage.setResizable(false);  ---  Non-resizable windows are bugged under Linux...
        primaryStage.show();
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
