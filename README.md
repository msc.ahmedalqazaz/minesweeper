Minesweeper
--
This repository contains a JavaFX implementation of
_minesweeper_. From a UI point of view, the choice
was made to mimic the classic Windows 2000 look.

The implementation follows the MVP pattern as seen
in _Programming 2_.

![./screenshots/shot1.png](./screenshots/shot1.png)

![./screenshots/shot2.png](./screenshots/shot2.png)
